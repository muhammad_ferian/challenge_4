class Car extends Component{
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor(props) {
      super(props);
      let { id, plate, manufacture, model, image, rentPerDay, capacity, description, transmission, available, type, year, options, specs, availableAt } = props;
      this.id = id;
      this.plate = plate;
      this.manufacture = manufacture;
      this.model = model;
      this.image = image;
      this.rentPerDay = rentPerDay;
      this.capacity = capacity;
      this.description = description;
      this.transmission = transmission;
      this.available = available;
      this.type = type;
      this.year = year;
      this.options = options;
      this.specs = specs;
      this.availableAt = availableAt;
    }

  render() {
    return `
      <div class="col mt-5 mb-5">
        <div class="card h-100">
            <img src="${this.image}" alt="${this.manufacture}" class="p-3 card-img-top" width="100%" height="300vh">
            <div class="card-body">
                <p>${this.manufacture} ${this.model}</p>
                <p><b>Rp ${this.rentPerDay} / Hari </b><p>
                <div class="card-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus iste autem voluptas adipisci soluta. 
                    Illo atque quidem, accusamus labore neque ipsum aut placeat quasi dolores! Dolores suscipit distinctio placeat molestias!</p>
                </div>
                <div class="row">
                    <div class="col-1">
                        <img src="images/image/fi_users.png" alt="" srcset="">
                    </div>
                    <div class="col-11">
                        <p class="text-start">${this.capacity} orang</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <img src="images/image/fi_settings.png" alt="" srcset="">
                    </div>
                    <div class="col-11">
                        <p class="text-start">${this.transmission}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-1">
                        <img src="images/image/fi_calendar.png" alt="" srcset="">
                    </div>
                    <div class="col-11">
                        <p class="text-start">Tahun ${this.year}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center">
                        <button type="button" class="btn btn-success padding text-center">Pilih Mobil</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
  }
}
