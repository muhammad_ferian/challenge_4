class App {
  constructor() {
    this.cariMobil = document.getElementById("btn-cari");
    this.carContainer = document.getElementById("cars-show");
    this.inputDriver = document.getElementById("input-driver");
    this.inputTanggal = document.getElementById("input-tanggal");
    this.inputWaktu = document.getElementById("input-waktu");
    this.inputPenumpang = document.getElementById("input-penumpang")
  }

  async init() {
    this.cariMobil.onclick = await this.click;
  }

  async loadFilter(filter) {
    const cars = await data.loadCarsFilter(filter);
    Car.init(cars);
  }

  click = async () => {
      let driver = this.inputDriver.value;
      let penumpang = this.inputPenumpang.value;
      let tanggal = this.inputTanggal.value;
      let waktu = this.inputWaktu.value;
      if (driver.lenght !== 0 && penumpang.lenght !== 0 && tanggal.lenght !== 0 && waktu.lenght !== 0) {
        tanggal = new Date(tanggal);
        await this.loadFilter({ driver, penumpang, tanggal, waktu})
      } else {
        alert('Isi Form Dulu')
      }
      this.cardRender();
    };
    
  cardRender() {
    let card = "";
    if (Car.list.length !== 0) {
      Car.list.forEach((car) => {
        card += car.render();
      });
      this.carContainer.innerHTML = card;
    } else {
      alert('Mobil Tidak Ditemukan')
      this.carContainer.innerHTML = card;
    }
  }
}
