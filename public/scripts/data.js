class data {
    static async loadCars() {
      let cars = await fetch("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json").then((response) => response.json());
      return cars;
    }
    static async loadCarsFilter({ driver, penumpang, tanggal, waktu }) {
      //   Get All The Cars
      let cars = await this.loadCars();
      let filterCars = cars

        .filter((car) => {
          if (car.available == true){
            return car;
          }
        })

        .filter((car) => {
          if (driver == "Dengan Supir"){
            return car;
          }
        })

        .filter((car) => {
          if (car.capacity >= penumpang){
            return car;
          }
        })

        .filter((car) => {
          let dateCar = new Date(car.availableAt);
          if (dateCar <= tanggal){
            return car;
          }
        })
        
      return filterCars;
    }
  }